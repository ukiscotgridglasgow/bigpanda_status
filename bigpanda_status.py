#! /usr/bin/python

import requests,json,time,socket

#################
# Carbon
#################

CARBON_SERVER = CARBONSERVER
CARBON_PORT = CARBONPORT
now = str(int(time.time()))
message = ''
prefix = "GRAPHITENAMESPACE"

#################
# Parse Arguments
#################

# BaseURL setup

baseurl = "http://bigpanda.cern.ch"

# API setup

api = "/status_summary/"

# Build API request

site="GLASGOW"

url = baseurl+api
payload = {'nhours': '12', 'computingsite': '*'+site+'*'}
headers = {'Accept': 'application/json', 'Content-Type': 'application/json'}

# API request

r = requests.get(url, headers=headers, params=payload)

# Processing

status_list = ['defined']
status_list.append('activated')
status_list.append('assigned')
status_list.append('throttled')
status_list.append('failed')
status_list.append('waiting')
status_list.append('holding')
status_list.append('sent')
status_list.append('closed')
status_list.append('transferring')
status_list.append('running')
status_list.append('finished')
status_list.append('merging')
status_list.append('pending')
status_list.append('cancelled')
status_list.append('starting')

queues = r.json()['data']

for queue in range(len(queues)):

    for status in status_list:
        queue_name = queues[queue]['computingsite']
        count = str(queues[queue][status])
        metric_prefix = prefix + "." + queue_name + "." + status
        message += metric_prefix + " " + count + " " + now + "\n"

sock = socket.socket()
sock.connect((CARBON_SERVER, CARBON_PORT))
sock.sendall(message)
sock.close()
