# bigpanda_status.py

**Imports**: requests,json,time,socket

This script is an example of parsing the JSON output from bigpanda.cern.ch/status_summary (API: https://github.com/PanDAWMS/panda-bigmon-core/blob/master/README-status_summary-API.md) to give a WLCG site job status for ATLAS. Currently assumes the use of [graphite](https://graphiteapp.org) (and defaults to GLASGOW) but should be flexible.